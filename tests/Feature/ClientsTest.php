<?php

namespace Tests\Feature;

use App\Client;
use Tests\TestCase;

class ClientsTest extends TestCase
{
    public function testGetClientAreCorrectly()
    {
        $client = factory(Client::class)->create([
            'first_name' => 'User 1',
            'last_name' => 'Last 1',
            'email' => 'user@example.com',
            'password' => '12345678',
        ]);
        $token = $client->generateToken();

        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/get-client/' . $client->id, [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $client->id,
                    'first_name' => 'User 1',
                    'last_name' => 'Last 1',
                    'email' => 'user@example.com',
                ]
            ]);
    }

    public function testGetClientsAreCorrectly()
    {
        $client1 = factory(Client::class)->create([
            'first_name' => 'User 1',
            'last_name' => 'Last 1',
            'email' => 'user1@example.com',
            'password' => '12345678',
        ]);
        $client2 = factory(Client::class)->create([
            'first_name' => 'User 2',
            'last_name' => 'Last 2',
            'email' => 'user2@example.com',
            'password' => '12345678',
        ]);
        $client3 = factory(Client::class)->create([
            'first_name' => 'User 3',
            'last_name' => 'Last 3',
            'email' => 'user3@example.com',
            'password' => '12345678',
        ]);
        $token = $client1->generateToken();

        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/get-clients', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    [
                        'id' => $client1->id,
                        'first_name' => 'User 1',
                        'last_name' => 'Last 1',
                        'email' => 'user1@example.com',
                    ],
                    [
                        'id' => $client2->id,
                        'first_name' => 'User 2',
                        'last_name' => 'Last 2',
                        'email' => 'user2@example.com',
                    ],
                    [
                        'id' => $client3->id,
                        'first_name' => 'User 3',
                        'last_name' => 'Last 3',
                        'email' => 'user3@example.com',
                    ],

                ]
            ]);
    }
}
