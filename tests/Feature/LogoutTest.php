<?php

namespace Tests\Feature;

use App\Client;
use Tests\TestCase;
use Illuminate\Foundation\Testing\WithFaker;
use Illuminate\Foundation\Testing\RefreshDatabase;

class LogoutTest extends TestCase
{
    public function testClientIsLoggedOutProperly()
    {
        $client = factory(Client::class)->create(['email' => 'user@test.com']);
        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $this->json('get', '/api/projects', [], $headers)->assertStatus(200);
        $this->json('post', '/api/logout', [], $headers)->assertStatus(200);

        $client = Client::find($client->id);

        $this->assertEquals(null, $client->api_token);
    }

    public function testClientWithNullToken()
    {
        // Simulating login
        $client = factory(Client::class)->create(['email' => 'user@test.com']);
        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        // Simulating logout
        $client->api_token = null;
        $client->save();

        $this->json('get', '/api/projects', [], $headers)->assertStatus(401);
    }
}
