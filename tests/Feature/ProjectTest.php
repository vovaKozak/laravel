<?php

namespace Tests\Feature;

use App\Project;
use App\Client;
use Tests\TestCase;

class ProjectTest extends TestCase
{
    public function testsProjectsAreCreatedCorrectly()
    {
        $client = factory(Client::class)->create();
        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $payload = [
            'name' => 'Lorem',
            'description' => 'Ipsum',
            'status' => Project::STATUSES[0]
        ];

        $this->json('POST', '/api/projects/create', $payload, $headers)
            ->assertStatus(201)
            ->assertJsonFragment(['name' => 'Lorem', 'description' => 'Ipsum', 'status' => Project::STATUSES[0], 'client_id' => $client->id]);
    }

    public function testsProjectsAreUpdatedCorrectly()
    {
        $client = factory(Client::class)->create();
        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $project = factory(Project::class)->create([
            'name' => 'First Project',
            'description' => 'First Body',
            'client_id' => $client->id
        ]);

        $payload = [
            'name' => 'Lorem',
            'description' => 'Ipsum',
        ];

        $response = $this->json('POST', '/api/projects/update/' . $project->id, $payload, $headers)
            ->assertStatus(200)
            ->assertJson([
                'id' => $project->id,
                'name' => 'Lorem',
                'description' => 'Ipsum',
                'client_id' => $client->id
            ]);
    }

    public function testsProjectsAreDeletedCorrectly()
    {
        $client = factory(Client::class)->create();
        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];
        $project = factory(Project::class)->create([
            'name' => 'First project',
            'description' => 'First descr',
            'client_id' => $client->id
        ]);

        $this->json('POST', '/api/projects/delete/' . $project->id, [], $headers)
            ->assertStatus(204);
    }

    public function testProjectsAreListedCorrectly()
    {
        $client1 = factory(Client::class)->create();
        $client2 = factory(Client::class)->create();

        factory(Project::class)->create([
            'name' => 'First Project',
            'description' => 'First description',
            'client_id' => $client1->id
        ]);

        factory(Project::class)->create([
            'name' => 'Second Project',
            'description' => 'Second description',
            'client_id' => $client1->id
        ]);

        factory(Project::class)->create([
            'name' => 'Third Project',
            'description' => 'Third description',
            'client_id' => $client2->id
        ]);


        $token = $client1->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/projects', [], $headers)
            ->assertStatus(200)
            ->assertJson([
                [ 'name' => 'First Project', 'description' => 'First description', 'client_id' => $client1->id],
                [ 'name' => 'Second Project', 'description' => 'Second description', 'client_id' => $client1->id],
                [ 'name' => 'Third Project', 'description' => 'Third description', 'client_id' => $client2->id]
            ])
            ->assertJsonStructure([
                '*' => ['id', 'name', 'description', 'status', 'client_id', 'created_at', 'updated_at'],
            ]);
    }

    public function testProjectOneCorrectly()
    {
        $client = factory(Client::class)->create();

        $project = factory(Project::class)->create([
            'name' => 'First Project',
            'description' => 'First description',
            'client_id' => $client->id
        ]);

        $token = $client->generateToken();
        $headers = ['Authorization' => "Bearer $token"];

        $response = $this->json('GET', '/api/projects/'.$project->id, [], $headers)
            ->assertStatus(200)
            ->assertJson([
                'data' => [
                    'id' => $project->id,
                    'name' => 'First Project',
                    'description' => 'First description',
                    'client_id' => $client->id
                ]
            ]);
    }
}
