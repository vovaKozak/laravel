<?php

namespace Tests\Feature;

use App\Client;
use Tests\TestCase;

class LoginTest extends TestCase
{

    public function testRequiresEmailAndLogin()
    {
        $this->json('POST', 'api/login')
            ->assertStatus(422)
            ->assertJson([
                'errors' => [
                    'email' => ['The email field is required.'],
                    'password' => ['The password field is required.'],
                ]
            ]);
    }


    public function testClientLoginsSuccessfully()
    {
        $client = factory(Client::class)->create([
            'email' => 'testlogin@user.com',
            'password' => bcrypt('12345678'),
        ]);

        $payload = ['email' => 'testlogin@user.com', 'password' => '12345678'];

        $this->json('POST', 'api/login', $payload)
            ->assertStatus(200)
            ->assertJsonStructure([
                'data' => [
                    'id',
                    'first_name',
                    'last_name',
                    'email',
                    'created_at',
                    'updated_at',
                    'api_token',
                ],
            ]);
    }
}
