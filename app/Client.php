<?php

namespace App;

use Illuminate\Notifications\Notifiable;
use Illuminate\Foundation\Auth\User as Authenticatable;

class Client extends Authenticatable
{
    use Notifiable;

    protected $table = 'clients';

    public $primaryKey = 'id';

    public $fillable = [
        'first_name', 'last_name', 'email', 'api_token'
    ];
    protected $hidden = [
        'password', 'api_token'
    ];

    public function projects()
    {
        return $this->hasMany('App\Project');
    }

    public function generateToken()
    {
        $this->api_token = str_random(60);
        $this->save();

        return $this->api_token;
    }

    public function getAuthPassword()
    {
        return $this->password;
    }

}
