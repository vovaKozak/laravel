<?php

namespace App;

use Illuminate\Database\Eloquent\Model;

class Project extends Model
{
    const STATUSES = [
        0 => 'planned',
        1 => 'running',
        2 => 'on_hold',
        3 => 'finished',
        4 => 'cancel'
    ];

    protected $table = 'project';

    protected $primaryKey = 'id';

    protected $fillable = [
        'name', 'description', 'status', 'client_id'
    ];

    public function client()
    {
        return $this->belongsTo('App\Client');
    }
}
