<?php

namespace App\Http\Controllers;

use App\Client;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;

class ClientsController extends Controller
{

    public function index()
    {
        $clients = Client::all();
        return response()->json(['data' => $clients->toArray()], 200);
    }

    public function create(Request $request)
    {
        $this->validator($request->all())->validate();
        $client = new Client();
        $client->first_name = htmlspecialchars($request['first_name']);
        $client->last_name = htmlspecialchars($request['last_name']);
        $client->email = htmlspecialchars($request['email']);
        $client->password = Hash::make($request['password']);
        $client->save();

        $client->generateToken();

        return response()->json(['data' => $client->toArray()], 201);
    }

    public function show(Client $client)
    {
        return response()->json(['data' => $client->toArray()], 200);
    }

    public function delete()
    {
        Auth::guard('clients-api')->user()->delete();
        return response()->json(['data' => 'Client removed.'], 200);
    }
}
