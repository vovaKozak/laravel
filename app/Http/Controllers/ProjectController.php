<?php

namespace App\Http\Controllers;

use App\Project;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;

class ProjectController extends Controller
{
    public function index()
    {
        return response()->json(Project::all()->toArray(), 200);
    }

    public function show(Project $project)
    {
        return response()->json(['data' => $project]);
    }

    public function store(Request $request)
    {
        $param = $request->all();

        $param['client_id'] = Auth::guard('clients-api')->id();
        $project = Project::create($param);

        return response()->json($project, 201);
    }

    public function update(Request $request, Project $project)
    {
        $project->update($request->all());

        return response()->json($project, 200);
    }

    public function delete(Project $project)
    {
        if ($project->client()->first()->id == Auth::guard('clients-api')->user()->id) {
            $project->delete();
        }

        return response()->json(null, 204);
    }
}
