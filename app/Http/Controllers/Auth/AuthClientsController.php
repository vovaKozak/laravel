<?php

namespace App\Http\Controllers\Auth;

use App\Client;
use App\Http\Controllers\Controller;
use Illuminate\Foundation\Auth\AuthenticatesUsers;
use Illuminate\Auth\AuthenticationException;
use Illuminate\Http\Request;
use Illuminate\Support\Facades\Auth;
use Illuminate\Support\Facades\Hash;
use Illuminate\Support\Facades\Validator;


class AuthClientsController extends Controller
{
    use AuthenticatesUsers;

    protected function guard()
    {
        return Auth::guard('clients');
    }

    protected function validator(array $data)
    {
        return Validator::make($data, [
            'first_name' => 'required|string|max:255',
            'last_name' => 'required|string|max:255',
            'email' => 'required|string|email|max:255|unique:users',
            'password' => 'required|string|min:6|confirmed',
        ]);
    }

    protected function unauthenticated($request, AuthenticationException $exception)
    {
        return response()->json(['error' => 'Unauthenticated'], 401);
    }

    public function register(Request $request)
    {
        $this->validator($request->all())->validate();
        $client = new Client();
        $client->first_name = htmlspecialchars($request['first_name']);
        $client->last_name = htmlspecialchars($request['last_name']);
        $client->email = htmlspecialchars($request['email']);
        $client->password = Hash::make($request['password']);
        $client->save();

        $token = $client->generateToken();
        $client = $client->toArray();
        $client['api_token'] = $token;

        return response()->json(['data' => $client], 201);
    }

    public function login(Request $request)
    {
        $this->validate($request, [
            'email' => 'required|string',
            'password' => 'required|string',
        ]);

        $credentials = $request->only('email', 'password');

        if ($this->guard()->attempt($credentials)) {
            $client = $this->guard()->user();
            $token = $client->generateToken();
            $client = $client->toArray();
            $client['api_token'] = $token;

            return response()->json([
                'data' => $client,
            ]);
        }

        return $this->sendFailedLoginResponse($request);
    }

    public function logout(Request $request)
    {
        $client = Auth::guard('clients-api')->user();
        if ($client) {
            $client->api_token = null;
            $client->save();
        }

        return response()->json(['data' => 'User logged out.'], 200);
    }





}
