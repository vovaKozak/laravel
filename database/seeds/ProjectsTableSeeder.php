<?php

use Illuminate\Database\Seeder;
use Illuminate\Support\Facades\DB;
use \App\Project;
use \App\Client;

class ProjectsTableSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        for ($i = 1; $i <= 3; $i++) {
            $clients = Client::all();

            foreach ($clients as $client) {
                for ($i = 0; $i <= 2; $i ++) {
                    DB::table('project')->insert([
                        'name' => 'Project '.$i,
                        'description' => 'Project users '. $client->id,
                        'status' => Project::STATUSES[rand(0,4)],
                        'client_id' => $client->id,
                        'created_at' => date("Y-m-d H:i:s"),
                        'updated_at' => date("Y-m-d H:i:s")
                    ]);
                }
            }
        }
    }
}
