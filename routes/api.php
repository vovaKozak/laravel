<?php


/*
|--------------------------------------------------------------------------
| API Routes
|--------------------------------------------------------------------------
|
| Here is where you can register API routes for your application. These
| routes are loaded by the RouteServiceProvider within a group which
| is assigned the "api" middleware group. Enjoy building your API!
|
*/

Route::post('register', 'Auth\AuthClientsController@register');

Route::post('login', 'Auth\AuthClientsController@login');



Route::group(['middleware' => 'auth:clients-api'], function() {
    Route::post('logout', 'Auth\AuthClientsController@logout');


    Route::get('get-client/{client}', 'ClientsController@show');
    Route::get('get-clients', 'ClientsController@index');
    Route::post('client/delete', 'ClientsController@delete');

    Route::get('projects', 'ProjectController@index');
    Route::get('projects/{project}', 'ProjectController@show');
    Route::post('projects/create', 'ProjectController@store');
    Route::post('projects/update/{project}', 'ProjectController@update');
    Route::post('projects/delete/{project}', 'ProjectController@delete');
});
